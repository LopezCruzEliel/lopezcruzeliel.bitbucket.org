var Ajedrez = (function() {
  console.log("in variable");
  var _mostrarTablero = function() {
    limparMensajes();
    console.log("entra en mostrarTablero");
    request();
  };

  var _actualizar_tablero = function() {
    limparMensajes();
    console.log("hola actualizar");
    var div_Tablero = document.getElementById("tablero");
    if (div_Tablero.childNodes[0] != null) {
      div_Tablero.removeChild(div_Tablero.childNodes[0]);
    }

    request();
  };

  var _mover_pieza = function(obj_parametros) {
    limparMensajes();
    var de = obj_parametros.de;
    var a = obj_parametros.a;
    var espacio_de = document.getElementById(de);
    var espacio_a = document.getElementById(a);
    console.log(espacio_de);
    console.log(espacio_a);
    if (espacio_de != null) {
      if (espacio_a != null) {
        var value_De = espacio_de.textContent;
        var value_A = espacio_a.textContent;
        if (value_De == value_A) {
          //si de y a son el mismo valor
          mensajes("debes seleciconar un destino ");
        } else {
          if (value_De != "") {
            if (value_A == "") {
              var temporal = value_A;
              espacio_a.textContent = value_De;
              espacio_de.textContent = temporal;

            } else mensajes("'a' debe ser un espacio vacio");
          } else mensajes("'de' debe ser una pieza en el tablero");
        }
        //si no exite el id en a o de
      } else mensajes("espacio 'a' no valido");
    } else mensajes("espacio 'de' no valido");
  };

  function request() {
    var url = "https://lopezcruzeliel.bitbucket.io/csv/tablero.csv"

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      if (this.readyState === 4) {
        if (this.status === 200) {
          var aje = this.responseText.split("\n");
          console.log(aje.length);

          if(aje.length=== 9){
            var estado= true;
            for (var i = 0; i < aje.length-1; i++) {
              console.log(aje[i].length);
              if(aje[i].length===15){

              }else{
                estado=false;
              }
            }

            if(estado===true){
             
              //validar_piezas(aje);
              if(validar_piezas(aje)===true){
                 crearTablero(aje); 
                 console.log(validar_piezas(aje));
              }else{
                mensajes("numero de piezas incorrecta");
              }
            }else{
              mensajes("archivo dañando");
            }
            
          }else{
            mensajes("archivo dañando");
          }
          //crearTablero(aje);
        } else {
          mensajes("error al descargar csv");
          console.log("error");
        }
      }
      Ajedrez
    };
    xhr.open('GET', url, true);
    xhr.send();
  }

  function crearTablero(datos) {
    var letra = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];
    var num = 9;
    var tablero = document.getElementById("tablero");
    var tabla = document.createElement("tabla");
    tabla.setAttribute("id", "tabla");
    tablero.appendChild(tabla);
    var tabla_body = document.createElement("tbody");
    tabla.appendChild(tabla_body);
    //asignar datos
    for (var i = 0; i < datos.length ; i++) {
      var tabla_row = document.createElement("tr");
      var fila = datos[i];
      var codePoint = fila.split("|");

      var tabla_dato = document.createElement("td");
      if (num != 9) tabla_dato.textContent = num;
      tabla_row.appendChild(tabla_dato);
      for (var j = 0; j < codePoint.length; j++) {
        var tabla_dato = document.createElement("td");
        if (i < 3 | i > 6) {
          tabla_dato.textContent = codePoint[j];
        } else tabla_dato.textContent = "";

        if (i > 0) tabla_dato.setAttribute("id", letra[j] + num);
        tabla_row.appendChild(tabla_dato);

      }

      tabla_body.appendChild(tabla_row);
      num--;
    }
  }

  function validar_piezas(datos){
    var rey="♔";
var reyna="♕";
var torre="♖";
var alfil= "♗";
var caballo="♘";
var peon="♙";

var reyB="♚";
var ReynaB = "♛"; 
var torreB= "♜";  
var alfilB= "♝" ;
var caballoB ="♞" ;
var peonB ="♟";
console.log("entra a validar");
console.log(rey);

var piezas = {
      torreN: 2,
      caballoN: 2,
      alfilN: 2,
      reyN: 1,
      reinaN: 1,
      peonN: 8,
      torreB: 2,
      caballoB: 2,
      alfilB: 2,
      reyB: 1,
      reinaB: 1,
      peonB: 8
    }
  var crey=0;var creyna=0;var ctorre=0;var calfil=0;var ccaballo=0; var cpeon=0;

    for (var i = 0; i < datos.length; i++) {
    var fila= datos[i]
    var lineas = fila.split("|");
    for (var j = 0; j < lineas.length; j++) {
      console.log(lineas[j]);
      if(lineas[j]===rey){
        piezas.reyN--;   
      }
      if(lineas[j]===reyna ){
        piezas.reinaN--;
      }
      if(lineas[j]===torre  ){
        piezas.torreN--;
      }
      if(lineas[j]===alfil ){
        piezas.alfilN--;
      }
      if(lineas[j]===caballo ){
        piezas.caballoN--;
      }
      if(lineas[j]===peon  ){
        piezas.peonN--;
      }
      if( lineas[j]===reyB){
        piezas.reyB--;
      }
      if (lineas[j]==ReynaB) {
        piezas.reinaB--;

      } if (lineas[j]===torreB) {
        piezas.torreB--;
        
      } if (lineas[j]===alfilB) {
        piezas.alfilB--;
      }
       if (lineas[j]===caballoB) {
        piezas.caballoB--;
      }
       if (lineas[j]===peonB) {
        piezas.peonB--;
      }
    }
    }


    if(piezas.peonN===0 && piezas.peonB===0 && piezas.torreN===0 && piezas.torreB===0 && piezas.caballoN===0 && piezas.caballoB===0
      && piezas.alfilN===0 && piezas.alfilB===0 && piezas.reyN===0 && piezas.reyB===0 && piezas.reinaB===0 && piezas.reinaN===0){
      //mensajes("numero de piezas correcta");
      return true;
    }else{
      return false;
      // mensajes("numero de piezas incorrecta");
       //console.log("rey : "+crey+"reyna: "+creyna+"ctorre"+ctorre+"alfin"+calfil+"caballo"+ccaballo+"peon"+cpeon);
    }
  }

  function mensajes(mensaje) {
    var div_mensaje = document.getElementById("mensaje");
    limparMensajes();
    var parrafo = document.createElement("p");
    parrafo.textContent = mensaje;
    div_mensaje.appendChild(parrafo);
  }
  return {
    "mostrarTablero": _mostrarTablero,
    "actualizarTablero": _actualizar_tablero,
    "moverPieza": _mover_pieza
  }

  function limparMensajes() {
    var div_mensaje = document.getElementById("mensaje");
    if (div_mensaje.childNodes[0] != null) {
      div_mensaje.removeChild(div_mensaje.childNodes[0]);
    }
  }

})();


//boton opcion
var opcion = document.getElementById("opcion");
var boton_option = document.createElement("button");
boton_option.textContent = "actualizar";
opcion.appendChild(boton_option);
boton_option.addEventListener('click', Ajedrez.actualizarTablero, true);
